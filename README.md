hsc3-unsafe
-----------

[haskell](http://haskell.org/)
[supercollider](http://audiosynth.com/)
([hsc3](http://rohandrape.net/?t=hsc3))
unsafe unit generator constructors.

Haskell provides a mechanism to force values
from the IO monad, `unsafePerformIO`.

Using this we can write unit generator graphs
that have non-deterministic nodes using only
orindary let binding.

    let u = System.IO.Unsafe.unsafePerformIO
        a = u (whiteNoiseM ar)
        b = u (whiteNoiseM ar)
        c = a - b
    in out 0 (c * 0.05)

Written as above this is hardly more convenient than do notation,
however we can also insert non-determinstic nodes directly into
function arguments.

     out 0 ((whiteNoise ar - whiteNoise ar) * 0.05)

The package `hsc3-unsafe` formerly provided unsafe variant unit generator
constructors with a `U` suffix.

These functions are now at `hsc3` without the `U` suffix.

© [rohan drape](http://rohandrape.net/), 2006-2022, [gpl](http://gnu.org/copyleft/)
