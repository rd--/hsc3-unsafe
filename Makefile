all:
	echo "hsc3-unsafe"

mk-cmd:
	echo "hsc3-unsafe - NIL"

clean:
	rm -Rf dist dist-newstyle *~

push-all:
	r.gitlab-push.sh hsc3-unsafe
