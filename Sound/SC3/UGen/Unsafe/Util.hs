-- * Util

{- | 'map' /f/ of (1..n).  This works when compiled by ghc-8.4.3

> cpy 3 id == [1,2,3]
> Sound.SC3.UGen.Unsafe.cpy 3 id == [1,2,3]
-}
cpy :: Int -> (Int -> t) -> [t]
cpy n f = map f [1 .. n]

{- | 'mce' of 'cpy'

> mce_cpy 2 (const (whiteNoiseU AR)) * 0.05
-}
mce_cpy :: Int -> (Int -> UGen) -> UGen
mce_cpy n = mce . cpy n

{- | Apply /f/ to () /n/ times.  This works when interpreted by ghc-8.4.3, but not when compiled.

> rpt 3 id  == [(),(),()]
> Sound.SC3.UGen.Unsafe.rpt 3 id == [(),(),()]
-}
rpt :: Int -> (() -> b) -> [b]
rpt n f = map f (replicate n ())
